package <++>;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * @author	<++>
 * @version 1.0
 * @since	<++>
 */
@Repository
public interface <++>Repository extends PagingAndSortingRepository<<++>, String> {

}
