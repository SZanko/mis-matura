# MIS-Matura

- Unsere Mitschriften und andere geistige Ergüsse
- Für **jede Datei** muss eine PDF erstellt werden und auch geupdated werden


# Ordner Struktur

# hp = Hoheheiser-Pförtner

hp/mitschriften = alle protokolle

Bei den unterschiedlichen Themenbereichen die Mitschriften in die anderen Unterlagen rein kopieren und umbenennen auf das Nummer_Thema_author_optionaledinge.dateiendung .


# Pirkers

pirker/folien = Pirkers Folien über die Jahre verteilt nach Thema sortiert
pirker/ausarbeitung = von Schülern nach Thema sortiert
Spring Skriptum liegt unter pirker/folien/spring-boot

# Spring-boot

Snippets für verschiedene Teile vom Backend

Benennung nummer_nachname_wasdasteilmacht
