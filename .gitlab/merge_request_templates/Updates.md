## What does this MR do?

<!-- Briefly described the result of your changes -->

## Why is this better than the current way?

<!-- https://about.gitlab.com/handbook/values/#say-why-not-just-what -->


## Checklist

- [ ] There is a new CHANGELOG entry under `master (unreleased)` that includes a link to this merge request
- [ ] The VERSION file is not modified
