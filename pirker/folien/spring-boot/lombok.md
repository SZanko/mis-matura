# Lombok features

::: {.row}
The [Lombok javadoc](/api/) is available, but we advise these pages.

::: {.bs-callout .bs-callout-danger}
#### [`val`](val)

Finally! Hassle-free final local variables.
:::

::: {.bs-callout .bs-callout-danger}
#### [`var`](var)

Mutably! Hassle-free local variables.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@NonNull`](NonNull)

or: How I learned to stop worrying and love the NullPointerException.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Cleanup`](Cleanup)

Automatic resource management: Call your `close()` methods safely with
no hassle.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Getter/@Setter`](GetterSetter)

Never write `public int getFoo() {return foo;}` again.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@ToString`](ToString)

No need to start a debugger to see your fields: Just let lombok generate
a `toString` for you!
:::

::: {.bs-callout .bs-callout-danger}
#### [`@EqualsAndHashCode`](EqualsAndHashCode)

Equality made easy: Generates `hashCode` and `equals` implementations
from the fields of your object..
:::

::: {.bs-callout .bs-callout-danger}
#### [`@NoArgsConstructor, @RequiredArgsConstructor and @AllArgsConstructor`](constructor)

Constructors made to order: Generates constructors that take no
arguments, one argument per final / non-nullfield, or one argument for
every field.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Data`](Data)

All together now: A shortcut for `@ToString`, `@EqualsAndHashCode`,
`@Getter` on all fields, and `@Setter` on all non-final fields, and
`@RequiredArgsConstructor`!
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Value`](Value)

Immutable classes made very easy.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Builder`](Builder)

\... and Bob\'s your uncle: No-hassle fancy-pants APIs for object
creation!
:::

::: {.bs-callout .bs-callout-danger}
#### [`@SneakyThrows`](SneakyThrows)

To boldly throw checked exceptions where no one has thrown them before!
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Synchronized`](Synchronized)

`synchronized` done right: Don\'t expose your locks.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@With`](With)

Immutable \'setters\' - methods that create a clone but with one changed
field.
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Getter(lazy=true)`](GetterLazy)

Laziness is a virtue!
:::

::: {.bs-callout .bs-callout-danger}
#### [`@Log`](log)

Captain\'s Log, stardate 24435.7: \"What was that line again?\"
:::

::: {.bs-callout .bs-callout-danger}
#### [`experimental`](/features/experimental/all)

Head to the lab: The new stuff we\'re working on.
:::
:::

::: {.row}
# Configuration system

::: {.text-center}
Lombok, made to order: [Configure lombok features](configuration) in one
place for your entire project or even your workspace.
:::
:::

::: {.row}
# Running delombok

<div>

Delombok copies your source files to another directory, replacing all
lombok annotations with their desugared form. So, it\'ll turn `@Getter`
back into the actual getter. It then removes the annotation. This is
useful for all sorts of reasons; you can check out what\'s happening
under the hood, if the unthinkable happens and you want to stop using
lombok, you can easily remove all traces of it in your source, and you
can use delombok to preprocess your source files for source-level tools
such as javadoc and GWT. More information about how to run delombok,
including instructions for build tools can be found at the [delombok
page](delombok).

</div>
:::

::: {.container}
[credits](/credits) \| Copyright © 2009-2021 The Project Lombok Authors,
licensed under the [MIT
license](http://www.opensource.org/licenses/mit-license.php).
:::
