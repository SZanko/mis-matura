# Vorwort

Manche Fragen wiederholen sich was laut dem Herrn Professor Pirker absichtlich ist.

Ps: Falls ihr ein **TODO** sieht fühlt euch frei das zu fixen

# 1. Beschreibe die Architektur unseres APIS?

In diesem Projekt kommt eine Server Client Architektur zum Einsatz.
Der Server stellt eine REST API über HTTP zu Verfügung, welche dann vom Webclient implementiert wird.
Die ganze API ist CRUD fähig.

**TODO** genauer Beschreibung von der Architektur

Unser APIS basiert auf einer Rest API, welche über das http Protokoll zu kontaktieren ist.
Wir implementieren eine CRUD fähige API


# 2. Liste die Technologien auf, die am Server zum Einsatz kommen. Beschreibe jeweils aus welchen Komponenten diese jeweils aufgebaut sind.

Auf dem Server verwenden wir Java und Spring Boot so wie Hibernate und eine SQL Datenbank, fürs Dependency Management wird Gradle mit Kotlin verwendet.
Zum Testen des Projektes wird Junit verwendet.
Java ist eine Programmiersprache, die keine Pointer hat, massiv auf OOP setzt und keinen Maschinen Code compiliert ,sondern in Bytecode compiliert für eine JVM.

Spring Boot, ist unser Webframework was Sockets und Ports für uns öffnet und bindet diese setzt auf Dependency injection.
Hibernate ist ein ORM Mapper welcher unsere Java Klassen, in die Datenbank mappt, das ganze funktioniert nach JPA.
MariaDB/H2 is unsere Datenbank welche unsere Daten speichert.
Gradle, pull alle dependencies, baut und testet außerdem das Projekt.
Jackson zum Mappen von Java Klassen nach JSON.
**TODO** genauer Beschreiben REST API Repository, Controller

# 3. Liste die Technologien auf, die am Client zum Einsatz kommen. Beschreibe jeweils aus welchen Komponenten diese jeweils aufgebaut sind.

In unseren Client kommt Angular mit Typescript zum Einsatz, ebenso npm mit NodeJS zum Dependency Management.
Wir haben zu jeder unserer Komponenten einen eigenen Test den wir nicht geschrieben haben.

Die Komponenten, die wir haben sind:

- Data Services um HTTP Requests zu machen
- Routing Component die bestimmt wohin URLs geroutet werden
- Module Component welche die anderen Komponenten miteinander bekannt macht
- View Components für die verschiedenen FHIR Ressourcen zum reinen Anzeigen und ungenauen erstellen
- View Components für die verschiedenen FHIR Ressourcen mit Bearbeitungsfunktion
- Modelklassen wohin das Resourcen JSON gemappt wird zur Benutzer in der View

# 4. Zähle auf und beschreibe, welche Protokolle verwendet werden, um Daten zwischen dem Server und dem Client zu übertragen.

- HTTP für die HTTP requests
- HTTPS wenn für Verschlüsselung zwischen Client Server.
<!--  - HTTP Methoden **TODO** Beschreibung von Methoden und Body -->

## Get Methode um alle Elemente einer Resource zu bekommen:
Der Client schickt diese Methode an Controller
    - Request Header:
    ```
    GET /api/patient/ HTTP/2
    Host: localhost:8080
    ```
    - Response Header:
        - 200
    - Request:
        - nichts
    - Response:
        - Resource

## Get Methode die ein Element einer Resource schickt:
Der Client schickt diese Methode an Controller
    - Header:
    ```
    GET /api/patient/Test HTTP/2
    Host: localhost:8080
    ```
    - Response Header:
        - 200
    - Request:
        - id der Sub Resource
    - Response:
        - Sub Resource mit der ID
## Post Methode
    - Header
    ```
    POST /api/patient/ HTTP/2
    Host: localhost:8080
    ```
    - Response Header:
        - 201
    - Request:
    ```
    {
        "id": "bla",
        "resourceType": "test",
        "identifier": [
            null
        ],
        "name": null,
        "telecom": null,
        "active": true,
        "gender": "unknown",
        "birthDate": "2021-04-07T14:18:28.390Z",
        "deceasedBoolean": false,
        "deceasedDateTime": "2021-04-07T14:18:28.390Z",
        "multipleBirthBoolean": false,
        "multipleBirthInteger": 3,
        "address": null
    }
    ```
    - Response:
        - Resource die erstellt wurde
## Put Methode
    - Request Header
    ```
    PUT /api/patient/05476f51-03f9-4f7b-b163-d2fe753481bb HTTP/2
    Host: localhost:8080
    ```
    - Response Header:
        - 200
    - Request Body
        - Upgedatete Resource
    - Response Body
        - Upgedatete Resource
## Delete Methode
    - Request Header:
    ```
    DELETE /api/patient/05476f51-03f9-4f7b-b163-d2fe753481bb HTTP/2
    Host: localhost:8080
    ```
    - Response Header:
        - 200
    - Request Body
        - kein
    - Response Body
        - kein
             
             

# 5. Beschreibe den Ablauf der Übertragung der Daten zwischen Client und Server.

1. Client stellt eine GET Request zu, um die Landing Page zu bekommen vom Server.
2. Server schickt dem Client eine Response mit den Daten als JSON welche gemappt werden vom Client.
3. Client schickt beim Erstellen einer neuen Instanz einer Resource eine POST REQUEST darauf folgt einen GET Request
4. Client schickt beim Updaten einer vorhandenen Instanz einer Resource eine PUT REQUEST darauf folgt einen GET Request
5. Client schickt beim Löschen einer vorhandenen Instanz einer Resource eine DELETE REQUEST darauf folgt einen GET Request

6. Welche Komponenten sind am Server und am Client für den Datenaustausch zuständig? Beschreibe jeweils die Funktionalität der Komponente.

- Am Client sind die HTTP Services zuständig welche nur HTTP Anfragen senden.
- Am Server sind die Rest Controller zuständig um eine Response zu senden und die Anfragen zu empfangen.

# 7. Welche Datenformate werden zwischen Client und Server ausgetauscht? Beschreibe exemplarisch den Aufbau eines der Dateneinheiten.

**TODO** besser Beschreibung dafür machen

- JSON
- Ps JSON hat keine Kommentare
- Aufbau
	- Hat Keys mit Values
- Beispiel für ein JSON
```
{
    "id": "Test",
    "active": true,
    "gender": "male",
    "birthDate": "2001-11-09",
    "identifier": [
        {
            "id": "230",
            "code": "secondary",
            "type": {
                "id": "123123",
                "coding": [],
                "text": "Test"
            },
            "system": "http://url",
            "value": "123123321",
            "period": {
                "start": "2007-09-09T00:00:00",
                "end": "2006-06-06T00:00:00"
            }
        }
    ],
    "name": [
        {
            "id": "123123",
            "use": "old",
            "text": "hun-text",
            "family": "hun-family",
            "given": [],
            "prefix": [],
            "suffix": [],
            "period": {
                "start": "1999-01-01T00:00:00",
                "end": "1999-01-01T00:00:00"
            }
        }
    ],
    "telecom": [],
    "deceased": "2001-11-09T00:00:00",
    "address": []
}

```

# 8. Welche http Endpoints gibt es am Server? Liste diese mit den notwendigen Kriterien tabellarisch auf.

Bei jeder Resource gibt es eine Methode(Response):

- Get(eine Id)
- GetALL(alle Ressourcen vom Typ)
- POST(JSON im Body von Request) darauf folgt eine GET Request
- PUT(JSON im Body von Request) darauf folgt eine GET Request
- DELETE(eine Id)

## Endpoints
- api/patient
- api/practitioner
- api/encounter
- api/flag
- api/deviceUseStatement
- api/medication

# 9. Beschreibe den Zusammenhang zwischen dem MVC Konzept und den verwendeten Komponenten am Server. Was würde dem M, was dem V und was dem C entsprechen?

- **M**odel = unsere Tabellen in der Datenbank die über das Repo angesprochen werden
- **V**iew = die ausgeliefert HTTP Response (laut Herr Professor Pirker)
- **C**ontroller = unsere Controller Klassen

# 10. Beschreibe die Aufgabe von FHIR im medizinischen Umfeld.

- HL7 Weiterentwicklung mit Webtechnologien
- **TODO** Beschreibung von den Unteren Zeug
- Personal Health Record
- Document Sharing
- Decision Support
- Explicit Requests for Decisions

# 11. Welche Protokolle gibt es, die ähnlich wie FHIR sind? Argumentiere, warum FHIR statt diesen verwendet wird.

- **TODO** Foliensatz implementieren von Moodle vom vierten Jahr
- HL7 v2 nicht für eine Webumgebung ausgelegt eigenes Dateiformat
- HL7 v3 nicht für eine Webumgebung ausgelegt verwendet xml

# 12. Argumentiere, welche Elemente unserer Applikation dem FHIR Standard entsprechen müssen. Warum genau diese und welche nicht? (Z.B. Datenbank, Übertragene Daten zwischen Client, Server, Oberfläche, Modelklassen, ...)

Nur unsere Responses/Requests müssen FHIR entsprechen alles andere, ist egal so, dass man auch andere Clients oder Server verwenden kann.

# 13. Argumentiere, warum es sinnvoll ist, den FHIR Standard einzusetzen.

- Weil sich alle denken, dass es sinnvoll ist neue Anwendungen als Webapplication um zu setzen &rarr; weniger Wartungsaufwand
- Sehr viele Entwickler für Webentwicklung
- Wenig HL7 v3 Entwickler
- Neue Möglichkeiten für personalisierte Medizin.

# 14. Beschreibe, welche Anwendungen durch FHIR für Endanwender (Patienten) ermöglicht werden?

- International transferverbare Patientakten
- **TODO** Beschreibung von den Unteren Zeug
- Personal Health Record
- Document Sharing
- Decision Support
- Explicit Requests for Decisions

# 15. Zähle zwei Beispiele für einen sinnvollen Einsatz von FHIR auf. Wo wird dabei FHIR eingesetzt?

- International transferverbare Patientakten
- Beschreibe das Zeug in der Prazis **TODO**
- Personal Health Record
- Document Sharing


# 16. Analysiere, wie unsere Applikation serverseitig abgesichert ist?

- Lauf nur Ports welche von außen nicht erreichbar sind.
- HTTPS
- SQL Injections
- Sonst gar nicht.

# 17. Welche Angriffsmöglichkeiten gibt es und welche Schwachstellen haben wir am Server?

- DDOS -> kein Load balancer
- Unerlaubtes Löschen schon Daten durch fehlende Authentifizierung, Autorisierung
- Physisches Zerstören des Rechners

# 18. Welche Maßnahmen wären sinnvoll, um unsere Applikation abzusichern?

- Load balancer
- Authentifizierung durch LDAP OAuth2
- Reverse-Proxy
- HTTPS
- Physischer Zugriff
- Theoretisch DB Passwort
- Time Keeping
- Autorisierung durch:
	- Role Base Access Controll
	- Attribute Base Access Controll
- Audit Logging
- Digital Signature
- Input Validation
